import LocalStrategy from 'passport-local';
import bcrypt from 'bcryptjs';
import FacebookStrategy from 'passport-facebook'
const Strategy = LocalStrategy.Strategy
// Load User model

export const _passport = function (passport) {
  passport.use(
    new Strategy({ usernameField: 'email' }, (email, password, done) => {
      // Match user
      User.findOne({
        email: email
      }).then(user => {
        if (!user) {
          return done(null, false, { message: 'That email is not registered' });
        }

        // Match password
        bcrypt.compare(password, user.password, (err, isMatch) => {
          if (err) throw err;
          if (isMatch) {
            return done(null, user);
          } else {
            return done(null, false, { message: 'Password incorrect' });
          }
        });
      });
    })
  );

  passport.serializeUser(function (user, done) {
    done(null, user.id);
  });

  passport.deserializeUser(function (id, done) {
    User.findById(id, function (err, user) {
      done(err, user);
    });
  });
  passport.use(new FacebookStrategy({
    clientID: 532258181290399,
    clientSecret: 'fa2460886a57c798ba0b481e9eb9e11e',
    callbackURL: "http://localhost:8080/auth/facebook/callback"
  },
    function (accessToken, refreshToken, profile, cb) {
      console.log(accessToken, refreshToken, profile)
      // User.findOrCreate({ facebookId: profile.id }, function (err, user) {
      //   
      // });
      return cb(null, { success: true });
    }
  ));

};
