export const serverErrorHandler = function(err, req, res, next) {
    const error = process.env.NODE_ENV !== 'prodction' ? err : {}
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: process.env.NODE_ENV !== 'prodction' ? err : {}
    });
}
export const serverNotFoundHandler = function(req, res, next) {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
}
export const serverStatus = (req, res) => res.send({
    status: "Up and running 1"
})