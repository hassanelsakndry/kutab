import renderResponseUtil from './../utils/RenderResponseUtil.js';
import messages from './../utils/customMessage.js';

const { sendResponse } = renderResponseUtil
const { ErrorMessage } = messages
export default function (req, res, next) {


    try {
        if (req.user.role == 'admin') return next()
        if (req.user._id !== req.params.userId) return _responseBuilder('you are not authorized to access this user', req, res)

        next();
    } catch (err) {
        return _responseBuilder(err.message, req, res)
    }
};


function _responseBuilder(error, req, res) {
    const errorMessage = new ErrorMessage(ErrorMessage.UNAUTHORIZED, error);
    return sendResponse(req, res, errorMessage)
}