import SecurityUtil from '../utils/SecurityUtil.js'
import UserService from '../components/users/service.js';
import renderResponseUtil from '../utils/RenderResponseUtil.js';
import messages from '../utils/customMessage.js';

const { sendResponse } = renderResponseUtil
const { decodeJWT } = SecurityUtil
const { ErrorMessage } = messages
export default async function(req, res, next) {


    try {
        if (!req.header('Authorization')) {
            return _responseBuilder('Please make sure your request has an Authorization header', req, res)
        }
        const token = req.header('Authorization');
        const payload = await decodeJWT(token);

        if (payload.tokenStatus === "Expired") {
            return _responseBuilder('Token is expired', req, res)
        }

        const user = await UserService.retreiveSingleUser(payload._id);
        if (!user.data) return _responseBuilder('this is not a user ', req, res)
            // if (!user.data.verified) {
            //     return _responseBuilder('user verification required', req, res)
            // }
        req.user = user.data;

        next();
    } catch (err) {
        return _responseBuilder(err.message, req, res)
    }
};


function _responseBuilder(error, req, res) {
    const errorMessage = new ErrorMessage(ErrorMessage.UNAUTHORIZED, error);
    return sendResponse(req, res, errorMessage)
}