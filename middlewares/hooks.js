import Ajv from 'ajv';
import renderResponseUtil from '../utils/RenderResponseUtil.js';
import normalise from 'ajv-error-messages';
import Messages from '../utils/customMessage.js';
const { ErrorMessage } = Messages
export default function(req, res, next) {
    req.validate = (schema, data) => {
        const ajv = new Ajv({ allErrors: true });
        const valid = ajv.validate(schema, data);
        if (!valid) {
            const errorMessage = new ErrorMessage(ErrorMessage.VALIDATING_OBJECT_ERROR, normalise(ajv.errors));
            renderResponseUtil.sendResponse(req, res, errorMessage)
        }
    };
    next();
};