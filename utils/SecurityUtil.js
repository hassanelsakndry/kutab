import jwt from 'jsonwebtoken';
import config from '../config/index.js';
import bcrypt from 'bcryptjs';
const { hash, compare } = bcrypt
export default {
    setPassword: async(password) => {
        const hashedPassword = await hash(password, config.bcrypt.saltRounds);
        return hashedPassword;
    },
    compareHashPassword: async(password, hash) => {
        const isPasswordsMatch = await compare(password, hash);
        return isPasswordsMatch;
    },
    createJWT: function createJWT(payload) {
        return jwt.sign(payload, config.jwt.secret, config.jwt.opts)
    },
    decodeJWT: function decodeJWT(token) {
        let verifyJWTPromise = new Promise((resolve, reject) => {
            jwt.verify(token, config.jwt.secret, config.jwt.opts, function(err, decoded) {
                if (err) {
                    if (err.name == 'TokenExpiredError') {
                        return resolve({ tokenStatus: "Expired" })
                    }
                    return reject(err);
                } else {
                    return resolve(decoded)
                }
            });
        });
        return verifyJWTPromise
    }
}