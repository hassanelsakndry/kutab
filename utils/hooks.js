import Ajv from 'ajv';
import Messages from './../utils/customMessage.js';
const { ErrorMessage, SuccessMessage } = Messages
import renderResponseUtil from './RenderResponseUtil'
export default (req, res, next) => {
    req.validate = (schema, data) => {
        const ajv = new Ajv({ allErrors: true });
        const valid = ajv.validate(schema, data);
        if (!valid) {
            const errorMessage = new ErrorMessage(ErrorMessage.VALIDATING_OBJECT_ERROR, ajv.errors);
            renderResponseUtil.sendResponse(req, res, errorMessage)
        }
    };
    next();
};