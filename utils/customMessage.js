import {set } from 'mongoose-error-handler'

class Message {}

class ErrorMessage extends Message {
    constructor(code, err) {
        super();
        if (err && err.name === 'MongoError') {
            err = (set(err)).errors
        }
        this.error = err;
        this.code = code;
        this.type = "Error";
    }
}


class SuccessMessage extends Message {
    constructor(code, data) {
        super();
        this.data = data;
        this.code = code;
        this.type = "Success";
    }
}

ErrorMessage.DATABASE_ERROR = "DATABASE_ERROR";
ErrorMessage.CREATING_OBJECT_ERROR = "CREATING_OBJECT_ERROR";
ErrorMessage.FINDING_OBJECT_ERROR = "FINDING_OBJECT_ERROR";
ErrorMessage.DELETING_OBJECT_ERROR = "DELETING_OBJECT_ERROR";
ErrorMessage.UPDATING_OBJECT_ERROR = "UPDATING_OBJECT_ERROR";
ErrorMessage.VALIDATING_OBJECT_ERROR = "VALIDATING_OBJECT_ERROR";
ErrorMessage.OBJECT_NOT_FOUND = "OBJECT_NOT_FOUND";
ErrorMessage.INVALID_USERNAME_PASSWORD = "INVALID_USERNAME_PASSWORD";
ErrorMessage.SENDING_MAIL_FAILURE = "SENDING_MAIL_FAILURE";
ErrorMessage.WRONG_VERIFICATION_CODE = "WRONG_VERIFICATION_CODE";
ErrorMessage.EXISTEDUSER = "EXISTEDUSER";
ErrorMessage.UNAUTHORIZED = "UNAUTHORIZED";
ErrorMessage.HALAQA_CLOSED = "HALAQA_IS_CLOSED";
ErrorMessage.HALAQA_NOT_EXISTED = "HALAQA_NOT_EXISTED";
ErrorMessage.TASK_ALREADY_CREATED = "TASK_ALREADY_CREATED";


SuccessMessage.CREATING_OBJECT_SUCCESS = "CREATING_OBJECT_SUCCESS";
SuccessMessage.DELETING_OBJECT_SUCCESS = "DELETING_OBJECT_SUCCESS";
SuccessMessage.UPDATING_OBJECT_SUCCESS = "UPDATING_OBJECT_SUCCESS";
SuccessMessage.GETTING_DATA = "GETTING_DATA";
SuccessMessage.AUTHENTICATION_SUCCESS = "AUTHENTICATION_SUCCESS";
SuccessMessage.SENDING_MAIL_SUCCESS = "SENDING_MAIL_SUCCESS";

export default { SuccessMessage, ErrorMessage }