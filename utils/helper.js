const EditableFields = ['bankAccount', 'companyRepresentative', 'driversCount', 'taxID', 'commercialID', 'address', 'phone2', 'phone1', 'name']
class Helper {
  queryUpdateFactor(updatableObj) {
    let _ = {}
    for (const field of EditableFields) {
      if (updatableObj[field] != null)
        _[field] = updatableObj[field]
    }
    return _
  }
  mockReq(req, res) {
    var response = {
      req: {
        body: { ...req.body },
        params: { ...req.params },
        query: { ...req.query }
      }
      ,
      res: {
        status: s => {
          return response.res
        },
        send: R => {
          response.res.data = R.data
          return response.res 
        },

      }
    }
    return {...response}
  }
}
export default new Helper()