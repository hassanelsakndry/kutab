import nodemailer from 'nodemailer';
export default {
    sendEmail
}


function sendEmail(subject, payload, endUsersEmails) {
    let supportTransporter = nodemailer.createTransport({
        host: "smtp.gmail.com",
        port: 465,
        secure: true, // secure:true for port 465, secure:false for port 587
        auth: {
            user: "ghaliaelsayegh@gmail.com",
            pass: "@P@ssw0rd"
        }
    });
    return new Promise((resolve, reject) => {
        send(supportTransporter, subject, payload, endUsersEmails, resolve, reject);
    });
}


function send(transporter, subject, payload, { sender, receiver }, resolve, reject) {

    const mailOptions = {
        from: sender || 'cicapitalconferences@cicapital.com', // sender address
        to: receiver || 'cicapitalconferences@cicapital.com', // list of receivers,
        text: payload,
        subject: subject, // Subject line
        html: payload // plain text body
    };
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            console.log("errrrrrrrrrr", error);
            reject("sending mail is failed");
        } else {
            resolve("Email has been sent successfully");
        }
    });
}