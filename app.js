import express from 'express';
import path from 'path';
import bodyParser from 'body-parser';
import { serverErrorHandler, serverNotFoundHandler, serverStatus }
from './middlewares/error-handler.js'
let app = express();

import packagesRouter from './components/packages/router.js';
import halaqatRouter from './components/halaqat/router.js';
import tasksRouter from './components/agent-tasks/router.js';
import auth from './auth/auth.js'
import usersRouter from './components/users/router.js';
// config()



const { ensureAuthenticated, forwardAuthenticated } = auth
// Welcome Page


import cors from 'cors'
app.use(cors())
app.use(bodyParser.json());

app.post('/test', (req, res) => res.send(req.body));

// import usersModule from './components/users/users.js'
// app.use('/users', usersModule)
app.use(['/api/halaqats', '/api/halaqat'], halaqatRouter)
app.use(['/api/tasks', '/api/task'], tasksRouter)
app.use(['/api/package', '/api/packages'], packagesRouter)
app.use(['/api/user', '/api/users'], usersRouter)

import { fileURLToPath } from 'url';
import { dirname } from 'path';

const __filename = fileURLToPath(
    import.meta.url);
const __dirname = dirname(__filename);
app.get('/status', serverStatus)
app.use(express.static(path.join(__dirname, "js")));
app.use(express.static(path.join(__dirname, '/dist')));
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, '/dist/index.html'));
})
app.use(serverNotFoundHandler);
app.use(serverErrorHandler);
app.set('port', process.env.PORT || 8080)

app.listen(app.get('port'), () => console.log(`server is running on Port 8080`))
export default app;