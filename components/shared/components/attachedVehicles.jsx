import { ApiClient } from 'admin-bro'
const api = new ApiClient()
import { Redirect } from 'react-router-dom'

import React, { Component } from 'react';

export default class Header extends Component {
    constructor(props) {
        super(props);
        const { record: initialRecord, resource, action } = props
        // const { record, handleChange, submit } = useRecord(initialRecord, resource.id);
        this.state = {
            companyId: initialRecord.id,
        }

    }
    render() {
        const attachedVehiclesFilterURL = `/admin/resources/vehicle?filters.owner=${this.state.companyId}&page=1`
        return <Redirect to={attachedVehiclesFilterURL} />
    }
}