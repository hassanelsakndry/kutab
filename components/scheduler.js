const schedule = require('node-schedule');
const http = require('http');
schedule.scheduleJob('0 */20 * * * *', async ()=>{
  try{
      console.log('this schedules runs every 20 minutes ');
      for (let index = 0; index <= 6; index++) {
        console.log(`event-scrapper-${index}.herokuapp.com`);
        await http.get({
          hostname: `event-scrapper-${index}.herokuapp.com`,
          path: '/',
          agent: false  // Create a new agent just for this one request
        });
      }
  }catch(err){
    console.log("scheduler error ==> ",err);
  }
});