import model from './model.js'
import messages from '../../utils/customMessage.js'
const { SuccessMessage, ErrorMessage } = messages
import helper from '../../utils/helper.js'

class Service {
    async create(payload) {
        const created = await model.create(payload)
        const message = new SuccessMessage(SuccessMessage.CREATING_OBJECT_SUCCESS, created);
        return message
    }
    async read(id) {
        const query = {}
        if (id) query._id = id
        const readed = await model.find(query)
        const message = new SuccessMessage(SuccessMessage, readed);
        return message
    }
    async readOne(id) {
        const query = {}
        if (id) query._id = id
        const readed = await model.findOne(query)
        const message = new SuccessMessage(SuccessMessage, readed);
        return message
    }
    async edit(_id, payload) {
        const _payload = helper.queryUpdateFactor(payload)
        const edited = await model.update({ _id }, { $set: {..._payload } })

        const message = new SuccessMessage(SuccessMessage, helper.updatedMessage(_payload));
        return message
    }
    async delete(id) {
        const deleted = await model.deleteOne({ _id: id })
        return new SuccessMessage(SuccessMessage.DELETING_OBJECT_SUCCESS, {});

    }
}
export default new Service()