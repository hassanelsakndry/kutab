import mongoose from 'mongoose';;
const { Schema } = mongoose


let packageSchema = mongoose.Schema({
    amount: {
        type: Number,
    },
    created: { type: Date, default: (() => new Date())() },
    currency: {
        type: String,
        default: 'egp'
    },
    isActive: {
        type: Boolean,
        default: true
    },
    company: {
        type:Schema.Types.ObjectId,
        ref:'company'
    }
});

export default mongoose.model('package', packageSchema)



