import express from 'express'
const router = express.Router();
import controller from './controller.js'

router.post('/', controller.create);
router.get('/', controller.read);
router.get('/:id', controller.readOne);
router.put('/:id', controller.edit);
router.delete('/:id', controller.delete)



export default router;