export default {
    createUser: {
        type: 'object',
        properties: {
            mobile: {
                type: 'string',
                minLength: 11,
                maxLength: 15
            },
            // password:{
            //     pattern: '^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{6,}$'
            // },
            username: {
                type: 'string',
                maxLength: 50
            }
        },
        messages: {
            type: 'Must be a number',
            minimum: 'No children...'
        },
        required: ['username', 'mobile'],
    },
    userLogin: {
        type: 'object',
        properties: {
            mobile: {
                type: 'string'
            },
            email: {
                type: 'string'
            },
            password: {
                type: 'string'
            }
        },
        required: ['password'],
    },
    sendEmail: {
        type: 'object',
        properties: {
            email: {
                format: "email"
            }
        }
    }
}