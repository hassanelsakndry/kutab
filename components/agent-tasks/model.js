import mongoose from 'mongoose';;
const { Schema } = mongoose


let task = mongoose.Schema({
    created: { type: Date, default: (() => new Date())() },
    agentId: Schema.Types.ObjectId,
    status: { type: String, default: 'open', enum: ['open', 'inreview', 'closed'] },
    studentId: Schema.Types.ObjectId,
    halaqaId: Schema.Types.ObjectId,
    taskType: { type: String, default: 'join_halaqa' },

});
let agent = mongoose.Schema({
    created: { type: Date, default: (() => new Date())() },
    userId: { type: Schema.Types.ObjectId, ref: 'user' },
    status: { type: String, default: 'open', enum: ['open', 'inreview', 'closed'] },
    fullName: {
        type: String
    },
    isSupervisor: {
        type: Boolean,
        default: false
    },
    creationDate: {
        type: Date,
        default: Date.now
    },
    isActive: {
        type: Boolean
    },
    isRobined: {
        type: Boolean,
        default: false
    },
    agentTypeEnum: {
        enum: ['male', 'female'],
        type: String
    }
});


export default {
    task: mongoose.model('task', task),
    agent: mongoose.model('agent', agent)
}