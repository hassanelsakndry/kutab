import service from './service.js'
import renderResponseUtil from '../../utils/RenderResponseUtil.js';

class Controller {
    async edit(req, res) {
        try {
            const { id } = req.params
            const edited = await service.edit(id, req.body);
            renderResponseUtil.sendResponse(req, res, edited)
        } catch (error) {
            console.log(error)
            renderResponseUtil.sendResponse(req, res, error)
        }
    }

    async readOne(req, res) {
        try {
            const readed = await service.readOne(req.params.id);
            return renderResponseUtil.sendResponse(req, res, readed)
        } catch (error) {
            renderResponseUtil.sendResponse(req, res, error)
        }
    }

    async read(req, res) {
        try {
            const readed = await service.read();
            renderResponseUtil.sendResponse(req, res, readed)
        } catch (error) {
            console.log(error)
            renderResponseUtil.sendResponse(req, res, error)
        }
    }

    async create(req, res) {
        try {
            const created = await service.create(req.body);
            renderResponseUtil.sendResponse(req, res, created)
        } catch (error) {
            console.log(error)
            renderResponseUtil.sendResponse(req, res, error)
        }
    }
    async addTask(task) {
        try {
            const agentId = await service.getRoundedAgent()
            return service.create(task, agentId);
        } catch (error) {
            throw Error(error)
        }
    }
    async handleBalanceImpact({ companyId, amount }) {
        try {
            const updatedBalance = await service.handleBalanceImpact({ companyId, amount })
            if (!updatedBalance.balance) throw Error('failed to update Balance !')
            console.log('update companyBalance =============>', updatedBalance);

        } catch (error) {
            console.log(error)
        }
    }
    async delete(req, res) {
        try {
            const { id } = req.params
            const deleted = await service.delete(id);
            renderResponseUtil.sendResponse(req, res, deleted)
        } catch (error) {
            console.log(error)
            renderResponseUtil.sendResponse(req, res, error)
        }
    }
    addAgent(agent) {
        return service.addAgent(agent)
    }
}
export default new Controller()