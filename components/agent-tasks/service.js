import model from './model.js'
import messages from '../../utils/customMessage.js'
const { SuccessMessage, ErrorMessage } = messages
import helper from '../../utils/helper.js'

class Service {
    async create(payload) {
        const { halaqaId, studentId } = payload
        const isCreatedBefore = await model.task.findOne({ halaqaId, studentId }).lean()
        if (isCreatedBefore) return ErrorMessage.TASK_ALREADY_CREATED
        const created = await model.task.create(payload)
        const message = new SuccessMessage(SuccessMessage.CREATING_OBJECT_SUCCESS, created);
        return message
    }
    async read(id) {
        const query = {}
        if (id) query._id = id
        const readed = await model.task.find(query)
        const message = new SuccessMessage(SuccessMessage, readed);
        return message
    }
    async readOne(id) {
        const query = {}
        if (id) query._id = id
        const readed = await model.task.findOne(query)
        const message = new SuccessMessage(SuccessMessage, readed);
        return message
    }
    async edit(_id, payload) {
        const _payload = helper.queryUpdateFactor(payload)
        const edited = await model.task.update({ _id }, { $set: {..._payload } })

        const message = new SuccessMessage(SuccessMessage, helper.updatedMessage(_payload));
        return message
    }
    async delete(id) {
        const deleted = await model.task.deleteOne({ _id: id })
        return new SuccessMessage(SuccessMessage.DELETING_OBJECT_SUCCESS, {});

    }
    getRoundedAgent() {
        return model.agent.findOne()
    }
    addAgent(agent) {
        agent.userId = agent._id
        agent.fullName = `${agent.firstname} ${agent.lastname}`
        return model.agent.create({...agent })
    }
}
export default new Service()