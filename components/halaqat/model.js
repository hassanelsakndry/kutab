import mongoose from 'mongoose';;
const { Schema } = mongoose


let halaqaSchema = mongoose.Schema({
    name: String,
    number: Number,
    students: [{
        type: Schema.Types.ObjectId,
        ref: 'user'
    }],
    teacher: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    },
    created: { type: Date, default: (() => new Date())() },
    isActive: {
        type: Boolean,
        default: true
    },
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    },
    status: { type: String, default: 'open', enum: ['open', 'closed'] },
    category: { type: String, enum: ["A", "B", "C"] }
});

export default mongoose.model('halaqa', halaqaSchema)