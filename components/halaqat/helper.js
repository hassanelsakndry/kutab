const EditableFields = ["name", "students", "teacher", "created", "isActive", "category"]
class Helper {
    queryUpdateFactor(updatableObj) {
        let _ = {}
        for (const field of EditableFields) {
            if (updatableObj[field] != null)
                _[field] = updatableObj[field]
        }
        return _
    }
}
export default new Helper()