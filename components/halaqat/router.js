import express from 'express'
const router = express.Router();
import controller from './controller.js'
import authenticate from '../../middlewares/AuthMiddleWare.js'
router.post('/', authenticate, controller.create);
router.get('/', controller.read);
router.get('/:id', controller.readOne);
router.put('/:id/:studentId', controller.join)
router.put('/:id', controller.edit);
router.delete('/:id', controller.delete)



export default router;