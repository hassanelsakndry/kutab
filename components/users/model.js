import mongoose from 'mongoose';;
const EMAIL_REGEXP = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
import bcrypt from 'bcrypt';
const SALT_WORK_FACTOR = 10;
const validateEmail = email => EMAIL_REGEXP.test(email);

let userSchema = mongoose.Schema({
    // username: {
    //     type: String,
    //     required: true,
    // },
    firstname: {
        type: String
    },
    lastname: {
        type: String
    },
    email: {
        type: String,
        lowercase: true,
        trim: true,
        validate: [validateEmail, 'Please fill a valid email address']
    },
    verified: {
        type: Boolean,
        default: false,
    },
    authyId: String,
    mobile: {
        trim: true,
        // required: true,
        default: (() => `${new Date()}`)(),
        type: String,
        unique: true
    },

    password: {
        type: String
    },
    description: {
        type: String
    },
    roles: [{
        type: String,
        enum: ["admin", "agent", "supervisor", "student", "teacher", "root"],
        default: 'student'
    }],
    verification: {
        code: String,
        attemptsNumber: {
            type: Number,
            default: 0
        },
        createdAt: {
            type: Date,
            default: (() => new Date())()
        }
    },

    androidDeviceId: [],
    iosDeviceId: [],
}, { timestamps: true });

userSchema.set('toObject', { virtuals: true })


userSchema.pre('save', function(next) {
    var user = this;

    // only hash the password if it has been modified (or is new)  
    if (!user.isModified('password')) return next();

    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
        if (err) return next(err);

        // hash the password using our new salt
        bcrypt.hash(user.password, salt, function(err, hash) {
            if (err) return next(err);

            // override the cleartext password with the hashed one
            user.password = hash;
            next();
        });
    });
});

userSchema.methods.comparePassword = function(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};


const users = mongoose.model('user', userSchema)

export default users;