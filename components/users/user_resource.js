const AdminBro = require('admin-bro')
const { sort, timestamps } = require('../shared/sort')

export default {
    name: 'Passengers',
    sort,
    properties: {
        ...timestamps,
    },
    actions: {
        detailedStats: {
            actionType: 'resource',
            icon: 'fas fa-signal',
            label: 'Resource statistics',
            component: AdminBro.require('../shared/components/detailed-stats'),
            handler: async(request, response, data) => {
                return { true: 'ueas' }
            },
        },
        dontTouchThis: {
            actionType: 'record',
            label: 'don\'t touch this!!!',
            icon: 'fas fa-exclamation',
            guard: 'You can setup guards before an action - just in case.',
            component: AdminBro.require('../shared/components/dont-touch-this-action'),
            handler: async(request, response, data) => {
                return {
                    record: data.record.toJSON()
                }
            }
        },
        test: {
            actionType: 'record',
            label: 'test',
            icon: 'fas fa-exclamation',
            guard: 'You can setup guards before an action - just in case.',
            component: AdminBro.require('../shared/components/dont-touch-this-action'),
            handler: async(request, response, data) => {
                return {
                    record: "test"
                }
            }
        },
    }
}

// 'user-ninja'