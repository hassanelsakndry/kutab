const EditableFields = ['username', 'firstname', 'lastname', 'email', 'verified', 'mobile', 'mobile2', 'description']
class Helper {
    queryUpdateFactor(updatableObj) {
        let _ = {}
        for (const field of EditableFields) {
            if (updatableObj[field] != null)
                _[field] = updatableObj[field]
        }
        return _
    }
    addMinutes(date, minutes) {
        let expirationDate = new Date(date)
        let timestamp = expirationDate.setMinutes(expirationDate.getMinutes() + minutes);
        return new Date(timestamp)
    }
}
export default new Helper()