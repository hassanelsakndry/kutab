import UserService from './service.js'
import renderResponseUtil from '../../utils/RenderResponseUtil.js'
import validation from '../validationSchemas.js'
import mailUtil from '../../utils/MailUtils.js'
import messages from '../../utils/customMessage.js'
import { authJwt, } from './auth.js'
import service from './service.js'
import agentController from '../agent-tasks/controller.js'
const { SuccessMessage, ErrorMessage } = messages

class Controller {
    async notify(req, res) {
        try {
            const user = await UserService.notify(req.body)
            renderResponseUtil.sendResponse(req, res, user)
        } catch (error) {
            renderResponseUtil.sendResponse(req, res, error)
        }
    }

    async resendPassword(req, res) {
        try {
            req.body.operation = req.query.operation
            const user = await UserService.resendPassword(req.body)
            user.accessToken = authJwt(user.data.user)
            renderResponseUtil.sendResponse(req, res, user)
        } catch (error) {
            renderResponseUtil.sendResponse(req, res, error)
        }
    }

    async addUserDevice(req, res) {
        try {
            const user = await UserService.addUserDevice(req.body)
            renderResponseUtil.sendResponse(req, res, user)
        } catch (error) {
            renderResponseUtil.sendResponse(req, res, error)
        }
    }

    async removeUserDevice(req, res) {
        try {
            const user = await UserService.removeUserDevice(req.body)
            renderResponseUtil.sendResponse(req, res, user)
        } catch (error) {
            renderResponseUtil.sendResponse(req, res, error)
        }
    }

    async editUser(req, res) {
        try {
            const { id } = req.params
            const user = await UserService.editUser(id, req.body)
            renderResponseUtil.sendResponse(req, res, user)
        } catch (error) {
            console.log(error)
            renderResponseUtil.sendResponse(req, res, error)
        }
    }

    async retreiveAllUsers(req, res) {
        try {
            const query = req.query
            const users = await UserService.retreiveAllUsers(query)
            renderResponseUtil.sendResponse(req, res, users)
        } catch (error) {
            renderResponseUtil.sendResponse(req, res, error)
        }
    }

    async retreiveSingleUser(req, res) {
        try {
            const user = await UserService.retreiveSingleUser(req.params.id)
            renderResponseUtil.sendResponse(req, res, user)
        } catch (error) {
            renderResponseUtil.sendResponse(req, res, error)
        }
    }

    async userLogin(req, res) {
        try {
            const userObj = req.body
            console.log(userObj)
                // req.validate(validation.userLogin, userObj)
            const user = await UserService.userLogin(userObj)

            renderResponseUtil.sendResponse(req, res, user)
        } catch (error) {
            console.log(error)
            const errorMessage = new ErrorMessage(ErrorMessage.SENDING_MAIL_FAILURE, error)

            renderResponseUtil.sendResponse(req, res, errorMessage)
        }
    }

    async createUser(req, res) {
        try {
            const userObj = req.body
            const user = await UserService.createUser(userObj)
            if (userObj.roles.includes('agent')) agentController.addAgent(user)
            renderResponseUtil.sendResponse(req, res, user)
        } catch (error) {
            console.log(error)
            renderResponseUtil.sendResponse(req, res, error)
        }
    }

    async sendMail(req, res) {
        try {
            req.validate(validation.sendEmail, req.body)
            const payload = `email : ${req.body.sender} <br> ${req.body.payload}`
            const subject = req.body.subject
            const sender = req.body.sender
            const mailRes = await mailUtil.sendEmail(subject, payload, { sender: sender })
            const message = new SuccessMessage(SuccessMessage.SENDING_MAIL_SUCCESS, mailRes)
            renderResponseUtil.sendResponse(req, res, message)
        } catch (error) {
            const errorMessage = new ErrorMessage(ErrorMessage.SENDING_MAIL_FAILURE, error)
            renderResponseUtil.sendResponse(req, res, errorMessage)
        }
    }
    async getVerficationCode(req, res) {
        try {
            const { id } = req.params
            const verficationCode = await service.setVerificationCode(id)
            const message = new SuccessMessage(SuccessMessage.SENDING_MAIL_SUCCESS, { verficationCode })
            renderResponseUtil.sendResponse(req, res, message)
        } catch (error) {
            console.log(error)
            const errorMessage = new ErrorMessage(ErrorMessage.SENDING_MAIL_FAILURE, error)
            renderResponseUtil.sendResponse(req, res, errorMessage)
        }
    }
    async verify(req, res) {
        try {
            const { id } = req.params
            const { verficationCode } = req.body
            const result = await service.verify(id, verficationCode)
            const message = new SuccessMessage(SuccessMessage.SENDING_MAIL_SUCCESS, result)
            renderResponseUtil.sendResponse(req, res, message)
        } catch (error) {
            console.log(error)
            const errorMessage = new ErrorMessage(ErrorMessage.WRONG_VERIFICATION_CODE, error.message)
            renderResponseUtil.sendResponse(req, res, errorMessage)
        }
    }
}

export default new Controller()