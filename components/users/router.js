import express from 'express';
import authMiddleware from '../../middlewares/AuthMiddleWare.js';
import userController from './controller.js';


const router = express.Router();


router.get('/status', (req, res) => {
    res.send('App is running ...')
})


router.post('/', userController.createUser);

router.post('/logout', userController.removeUserDevice);
router.get('/', userController.retreiveAllUsers)

// router.post('/device',userController.addUserDevice);
router.get('/:id/verificationCode', userController.getVerficationCode)
router.post('/:id/verify', userController.verify)
// router.post('/notifications',userController.notify);

router.get('/user/:id', authMiddleware, userController.retreiveSingleUser);

// router.get('/user',userController.retreiveAllUsers);

// router.put('/user',userController.editUser);
router.put('/:id', userController.editUser);

router.post('/signin', userController.userLogin);

// router.post('/contact',userController.sendMail);

// router.delete('/session',userController.resendPassword);



export default router;



// upload image  notification
// user managment
// 