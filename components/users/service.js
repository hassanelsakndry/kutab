import UserSchema from './model.js';
import messages from '../../utils/customMessage.js';
import cloudinary from 'cloudinary';
import serurityUtils from '../../utils/SecurityUtil.js'
import helper from './helper.js'
import mailUtil from '../../utils/MailUtils.js'
import notificationUtil from '../../utils/notificationUtil.js';
// import phoneToken from 'generate-sms-verification-code'

const model = UserSchema





const { setPassword, compareHashPassword, createJWT } = serurityUtils
const { queryUpdateFactor, addMinutes } = helper



const { ErrorMessage, SuccessMessage } = messages
cloudinary.config({
    cloud_name: 'cairo-boutique',
    api_key: '143127488698556',
    api_secret: 'vwTM_ZcGXoST5kf1F2qUfy5daP0'
});




class Service {

    async createUser(userObj) {
        const { email, mobile, username, roles } = userObj
        const $or = []
        if (email)
            $or.push({ email })
        if (mobile)
            $or.push({ mobile })
        if (username)
            $or.push({ username })
        const existsUser = await UserSchema.findOne({ $or }).lean(true)
        console.log('existsUser =============>', existsUser);
        if (existsUser) throw new ErrorMessage(ErrorMessage.EXISTEDUSER, 'this User is Already Existed');
        const response = (await UserSchema.create(userObj)).toObject();
        delete response.password;
        const message = new SuccessMessage(SuccessMessage.CREATING_OBJECT_SUCCESS, { user: response, accessKey: createJWT(response) });
        return message

    }





    notify(userObj) {
        return new Promise(async(resolve, reject) => {
            try {
                await UserSchema.findOne({ attendeeId: userObj.attendeeId }).lean().exec(function(err, doc) {
                    if (doc) {
                        let notificationDevices = {
                            iosDevices: doc.iosDeviceId,
                            androidDevices: doc.androidDeviceId
                        };
                        notificationUtil.sendNotification('your schedule has been updated.', notificationDevices, { screen: "scheduleUpdate" });
                        const message = new SuccessMessage(SuccessMessage.SENDING_MAIL_SUCCESS, {});
                        return resolve(message);
                    }
                    const errorMessage = new ErrorMessage(ErrorMessage.SENDING_MAIL_FAILURE, 'cannot send notification');
                    reject(errorMessage);
                });

            } catch (error) {
                const errorMessage = new ErrorMessage(ErrorMessage.SENDING_MAIL_FAILURE, error);
                reject(errorMessage);
            }
        })
    }

    resendPassword(userObj) {
        return new Promise(async(resolve, reject) => {

            try {
                if (userObj.operation == 'resend') {
                    UserSchema.findOne({
                            $or: [{
                                    mobile: userObj.mobile
                                },
                                { mobile2: userObj.mobile },
                                {
                                    email: userObj.email
                                }
                            ]
                        })
                        .then(async(foundedUser) => {
                            if (foundedUser) {
                                const mailRes = await mailUtil.sendEmail("CICAPITAL Password Reminder", "Based on your request, kindly find below the username and password <br> <br> username: " + foundedUser.username + "<br> password: " + foundedUser.password, { receiver: foundedUser.email })
                                const message = new SuccessMessage(SuccessMessage.SENDING_MAIL_SUCCESS, {});
                                resolve(message);
                            } else {
                                const mailRes =
                                    await mailUtil.sendEmail("CICAPITAL Password Reminder", "user has requested password reminder with email : " + userObj.email + "<br> and mobile: " + userObj.mobile + ' and it is not found in our database', { receiver: 'cicapitalconferences@cicapital.com' })
                                const message = new SuccessMessage(SuccessMessage.SENDING_MAIL_SUCCESS, {});
                                resolve(message);
                            }
                        })
                        .catch((error) => {
                            const errorMessage = new ErrorMessage(ErrorMessage.FINDING_OBJECT_ERROR, error);
                            reject(errorMessage);
                        });
                }
            } catch (error) {
                const errorMessage = new ErrorMessage(ErrorMessage.FINDING_OBJECT_ERROR, error);
                reject(errorMessage);
            }
        })
    }

    addUserDevice(userObj) {
        return new Promise(async(resolve, reject) => {
            try {
                const deviceIos = userObj.os ? userObj.os.toLowerCase() == 'android' ? 'androidDeviceId' : 'iosDeviceId' : 'iosDeviceId';
                updateObj = {
                    'key': userObj.notificationDevice
                }
                updateObj[deviceIos] = updateObj['key'];
                const users = await UserSchema.findOneAndUpdate({ _id: userObj.id }, { $addToSet: updateObj }, {
                    new: true
                });
                const message = new SuccessMessage(SuccessMessage.UPDATING_OBJECT_SUCCESS, users);
                resolve(message);
            } catch (error) {
                const errorMessage = new ErrorMessage(ErrorMessage.UPDATING_OBJECT_ERROR, error);
                reject(errorMessage);
            }
        })
    }

    removeUserDevice(userObj) {
        return new Promise(async(resolve, reject) => {
            try {
                const users = await UserSchema.findOneAndUpdate({ _id: userObj.id }, { $pullAll: { notificationDevices: [userObj.notificationDevice] } }, {
                    new: true
                })
                const message = new SuccessMessage(SuccessMessage.UPDATING_OBJECT_SUCCESS, users);
                resolve(message);
            } catch (error) {
                const errorMessage = new ErrorMessage(ErrorMessage.UPDATING_OBJECT_ERROR, error);
                reject(errorMessage);
            }
        })
    }

    retreiveAllUsers(query) {
        return new Promise(async(resolve, reject) => {
            try {
                const users = await UserSchema.find({...query }, { password: 0 }).lean();
                const message = new SuccessMessage(SuccessMessage.GETTING_DATA, users);
                resolve(message);
            } catch (error) {
                const errorMessage = new ErrorMessage(ErrorMessage.FINDING_OBJECT_ERROR, error);
                reject(errorMessage);
            }
        })
    }

    retreiveSingleUser(userId) {
        return new Promise(async(resolve, reject) => {
            try {

                const user = await UserSchema.findOne({
                    _id: userId
                }, { password: 0 });
                const message = new SuccessMessage(SuccessMessage.GETTING_DATA, user);
                resolve(message);
            } catch (error) {
                const errorMessage = new ErrorMessage(ErrorMessage.FINDING_OBJECT_ERROR, error);
                reject(errorMessage);
            }
        })
    }

    // function retreiveSingleUser(userId) {
    //   return new Promise(async (resolve, reject) => {
    //     try {
    //       const user = await UserSchema.find({
    //         _id: userId
    //       }).populate('company');
    //       const message = new SuccessMessage(SuccessMessage.GETTING_DATA, user);
    //       resolve(message);
    //     } catch (error) {
    //       const errorMessage = new ErrorMessage(ErrorMessage.FINDING_OBJECT_ERROR, error);
    //       reject(errorMessage);
    //     }
    //   })
    // }

    async userLogin(userObj) {
        const handleError = () => { throw (new ErrorMessage(ErrorMessage.INVALID_USERNAME_PASSWORD, 'invalid mobile number or password')) }
        const query = { $or: [] }



        if (userObj.email) query.$or.push({ 'email': userObj.email })
            // if (userObj.mobile) query.$or.push({ 'mobile': userObj.mobile })
        const foundedUser = await UserSchema.findOne(query)
        if (!foundedUser) { return handleError(); }




        return new Promise((resolve, reject) => {
            const matchPasswordCB = (err, isMatch) => {
                console.log({ err, isMatch })
                if (err) throw err;
                if (!isMatch) { reject(handleError()); }
                const clonedfoundedUser = JSON.parse(JSON.stringify(foundedUser))
                delete clonedfoundedUser.password;
                let restult = new SuccessMessage(SuccessMessage.AUTHENTICATION_SUCCESS, { user: clonedfoundedUser, accessKey: createJWT(clonedfoundedUser) })
                console.log({ restult })
                resolve(restult);
            }
            foundedUser.comparePassword(userObj.password, matchPasswordCB)
        })


    }


    async editUser(id, userObj) {
        console.log(id, userObj)
        const update = queryUpdateFactor(userObj)
        const updatedUser = await UserSchema.findOneAndUpdate({ _id: id }, { $set: {...update } }, { new: true }, )
        delete updatedUser.password
        return updatedUser
    }


    _updateFactory(userObj, uploadingResult) {
        const updateParams = {
            "username": userObj.username,
            "password": userObj.password,
            "firstname": userObj.firstname,
            "lastname": userObj.lastname,
            "email": userObj.email,
            "mobile": userObj.mobile,
            "mobile2": userObj.mobile2,
            "company": userObj.company,
            "role": userObj.role,
            "title": userObj.title,
            "description": userObj.description,
            "schedule": {
                path: ""
            },
            // "$addToSet":{
            //   "notificationDevices":userObj.notificationDevice
            // }
        }
        if (userObj.files && userObj.files.pdf && userObj.files.pdf.length > 0) {
            updateParams.schedule.path = uploadingResult.url
        } else {
            updateParams.schedule = null;
        }
        Object.keys(updateParams).forEach((key) => (updateParams[key] == null) && delete updateParams[key]);
        return updateParams;
    }
    async setVerificationCode(_id) {
        // var code = phoneToken(6, { type: 'number' })
        // await model.update({ _id }, {
        //     $set: {
        //         verification: {
        //             code,
        //             createdAt: new Date(),
        //             attemptsNumber: 0
        //         }
        //     }
        // })

        // return code
    }
    async verify(_id, code) {
        const user = await model.findOne({ _id })
        if (user.verification.code != code) throw Error('this code is not correct !')
        if (!user) throw Error('this is user is not existed !')
        if (user.verified) throw Error('User Is Aleardy Verified !')
        if (user.verification.attemptsNumber > 3) throw Error('you have exceed the limit attempts Number !')
        const ExpiredDate = addMinutes(user.verification.createdAt, 1)
        if (new Date() > ExpiredDate) throw Error('verification code is Expired !')
        user.verified = true
        user.verification.attemptsNumber++;
        const saved = await user.save()
        console.log(saved)
        return `user is verified successfully ..!`
    }
}
export default new Service()