export default {
    "local": {
        "jwt": {
            "secret": "7arakasecret",
            "opts": {
                "expiresIn": "1h",
                "algorithm": "HS512"
            },
            "refreshToken": {
                "expiresIn": "100d"
            }
        },
        "DB_CONNECTION_URL": "mongodb+srv://alkutabUser:ticC23YznNmb64W2@cluster0.kkstm.mongodb.net/alkutab?retryWrites=true&w=majority",
        "bcrypt": {
            "saltRounds": 10
        }
    },
    "production": {
        "jwt": {
            "secret": "7arakasecret",
            "opts": {
                "expiresIn": "1h",
                "algorithm": "HS512"
            },
            "refreshToken": {
                "expiresIn": "100d"
            }
        },
        "JWTSECRET": "EVENTAPPSECRET",
        "DB_CONNECTION_URL": "mongodb+srv://alkutabUser:ticC23YznNmb64W2@cluster0.kkstm.mongodb.net/alkutab?retryWrites=true&w=majority",

        "bcrypt": {
            "saltRounds": 10
        }
    },
    "testDB": "mongodb://adminTest:QWErty1@ds125365.mlab.com:25365/alkutab"
}