import User from '../components/users/model.js';

var user = {
    username: "Alkutab admin",
    email: "basmaelsakndry@gmail.com",
    password: "P@ssw0rd&12345",
    role: "admin",
    mobile: '01279029552'
}


export default async function seedAdmin() {
    try {
        const isExist = await User.findOne({ email: user.email });
        if (!isExist) {
            return await User.create(user);
        }
    } catch (error) {
        console.log("seeding admin user error :: ", error);
    }
}