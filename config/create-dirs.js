var fs = require('fs');
const path = require('path');

exports.createDirs = () => {
    const dirs = ['./static', './static/images', './static/pdf']
    dirs.forEach(dir => {
        dir = path.join(__dirname, dir)
        if (!fs.existsSync(dir)) fs.mkdirSync(dir);
    })
}

 
