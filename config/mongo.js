import mongoose from 'mongoose';
export default config => {
    mongoose.connect(config.DB_CONNECTION_URL, { useUnifiedTopology: true, useNewUrlParser: true,retryWrites:false })
        .then(() => {
            console.log("connected to db successfully");
        })
        .catch((error) => {
            console.log(error);
            process.exit(1);
        });
}