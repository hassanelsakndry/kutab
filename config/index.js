import config from "./config.js"
let currentConfig = config[process.env.NODE_ENV || 'local'];
import mongo from './mongo.js'
mongo(currentConfig)
import seedAdmin from './seed.js'
seedAdmin()
export default config['local'];